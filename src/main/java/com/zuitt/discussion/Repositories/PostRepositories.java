package com.zuitt.discussion.Repositories;


import com.zuitt.discussion.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepositories extends CrudRepository<Post,Object> {

}
